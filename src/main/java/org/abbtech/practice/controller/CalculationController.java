package org.abbtech.practice.controller;

import org.abbtech.practice.dto.CalculationDto;
import org.abbtech.practice.input.ApiRequest;
import org.abbtech.practice.service.ApplicationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator-api")
public class CalculationController {

    private final ApplicationService applicationService;

    public CalculationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @PostMapping("/calculate")
    public CalculationDto calculate(@RequestBody ApiRequest apiRequest){
        int a = apiRequest.getA();
        int b = apiRequest.getB();
        String method = apiRequest.getMethod();
        int result = 0;

        switch (method) {
            case "multiply":
                result = applicationService.multiply(a, b);
                break;
            case "add":
                result = applicationService.add(a, b);
                break;
            case "divide":
                result = applicationService.divide(a, b);
                break;
            case "subtract":
                result = applicationService.subtract(a, b);
                break;
            default:
                throw new ArithmeticException("not a valid operator");
        };
        return new CalculationDto(result);
    }
}
