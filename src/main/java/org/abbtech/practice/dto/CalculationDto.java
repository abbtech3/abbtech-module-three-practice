package org.abbtech.practice.dto;

public record CalculationDto(int result) {
}
