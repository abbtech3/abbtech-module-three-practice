package org.abbtech.practice.input;

public class ApiRequest {
    private Integer a;
    private Integer b;
    private String method;

    public ApiRequest(Integer a, Integer b, String method) {
        this.a = a;
        this.b = b;
        this.method = method;
    }

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
